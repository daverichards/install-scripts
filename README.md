# Install scripts

## Git / Curl

```
sudo apt-get update
sudo apt-get install -y curl git
```

## Chrome

```
sudo apt-get install libxss1 libappindicator1 libindicator7
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb
```

## Set up ssh keys in bitbucket

Generate default ssh key:
```
ssh-keygen -t rsa -C "dave.richards@gmail.com"
```

Get the public key to add to bitbucket:
```
cat ~/.ssh/id_rsa.pub
```

## Pull dotfiles

```
echo ".cfg" >> .gitignore
git clone --bare git@bitbucket.org:daverichards/cfg.git $HOME/.cfg
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
mv .bashrc .bashrc_orig
config checkout
config config --local status.showUntrackedFiles no
```

## Pull installation scripts

```
git clone git@bitbucket.org:daverichards/install-scripts.git $HOME/install-scripts
```

Run the install script:
```
sudo ~/install-scripts/install-all
```

### PhpStorm

Download from: https://www.jetbrains.com/phpstorm/download/#section=linux

```
sudo tar -C /opt -xzf PhpStorm-*.tar.gz
sudo chgrp -R dev /opt/PhpStorm-*
sudo chmod -R g+w /opt/PhpStorm-*
```

run `phpstorm.sh` in the `/opt/PhpStorm-*/bin/` dir.

### Slack

Download instructions: https://slack.com/downloads/instructions/linux

### Docker

https://www.docker.com/community-edition#/download

On Ubuntu I generally have to go via the manual installation from a package route.

Docker Compose: https://docs.docker.com/compose/install/

## Clone Repos

### tastecard (legacy)

```
sudo mkdir -p /srv/www/tastecard
sudo chgrp -R dev /srv/www
sudo chmod -R g+w /srv/www
git clone git@bitbucket.org:tastecard/tastecard.git /srv/www/tastecard/tastecard
git clone git@bitbucket.org:tastecard/tastecard-admin.git /srv/www/tastecard/tastecard-admin
git clone git@bitbucket.org:tastecard/server-management.git /srv/www/tastecard/server-management
git clone git@bitbucket.org:tastecard/payment.tastecard.co.uk.git /srv/www/tastecard/payment-gateway

```

### DCG

```
sudo mkdir -p /srv/www/dining-club-group
sudo chgrp -R dev /srv/www/dining-club-group
sudo chmod -R g+w /srv/www/dining-club-group
git clone git@bitbucket.org:tastecard/cinema-api.git /srv/www/dining-club-group/cinema-api
git clone git@bitbucket.org:tastecard/cinema-web.git /srv/www/dining-club-group/cinema-web
```

